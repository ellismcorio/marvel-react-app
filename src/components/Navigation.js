import React from 'react';
import { NavLink } from 'react-router-dom';


const Navigation = () => {
    return (
        <div>
            <NavLink to="/">Home</NavLink>
            <NavLink to="/comics">Comics</NavLink>
            <NavLink to="/series">Series</NavLink>
            <NavLink to="/stories">Stories</NavLink>
            <NavLink to="/events">Events</NavLink>
            <NavLink to="creators">Creators</NavLink>
            <NavLink to="characters">Characters</NavLink>
        </div>
    )
}

export default Navigation;