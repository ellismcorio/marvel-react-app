import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Home from './components/Home';
import Comics from './components/Comics';
import Series from './components/Series';
import Stories from './components/Stories';
import Events from './components/Events';
import Creators from './components/Creators';
import Characters from './components/Characters';
import Error from './components/Error';
import Navigation from './components/Navigation';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Navigation />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/comics" component={Comics} />
            <Route path="/series" component={Series} />
            <Route path="/stories" component={Stories} />
            <Route path="/events" component={Events} />
            <Route path="/creators" component={Creators} />
            <Route path="/characters" component={Characters} />
            <Route exact component={Error} />
          </Switch>
        </div>  
      </BrowserRouter>
     
    );
  }
}

export default App;
